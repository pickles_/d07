//
// Created by dtitenko on 11/9/17.
//

#ifndef D07_ARRAY_HPP
#define D07_ARRAY_HPP

#include <glob.h>
#include <cstdlib>
#include <exception>
template <class T = int>
class Array
{
	private:
		unsigned int _size;
		T *_array;

	public:
		Array<T>() : _size(0), _array(NULL){}
		~Array<T>()
		{
			if (_array)
				delete [] _array;
		}

		Array<T>(unsigned int n): _size(n)
		{
			_array = new T[n];
		}

		Array<T>(const Array<T> &array): _size(0), _array(NULL)
		{ *this = array; }

		Array<T> &operator=(const Array<T> &rhs)
		{
			if (this == &rhs)
				return (*this);
			delete [] _array;
			_size = rhs._size;
			if (_size == 0)
				_array = NULL;
			else
				_array = new T[_size];
			for (unsigned int i = 0; i < _size; i++)
				_array[i] = rhs._array[i];
			return *this;
		}

		T &operator[](unsigned int n)
		{
			if (n < _size)
				return _array[n];
			else
				throw std::exception();
		}

		unsigned int size() const
		{
			return _size;
		}
};

#endif //D07_ARRAY_HPP

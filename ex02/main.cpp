//
// Created by dtitenko on 11/9/17.
//
#include <iostream>
#include "Array.hpp"

int main()
{
	unsigned int i = 9;
	Array<int> arr1 = Array<int>(i);

	std::srand(time(NULL));

	for (unsigned int j = 0; j < arr1.size(); j++)
		std::cout << arr1[j] << std::endl;
	std::cout << std::endl;
	for (unsigned int j = 0; j < arr1.size(); j++)
		arr1[j] = j << std::rand() % 16;
	try
	{
		arr1[9] = 42;
	}
	catch (std::exception &e)
	{
		std::cout << e.what() << std::endl;
	}

	for (unsigned int j = 0; j < arr1.size(); j++)
		std::cout << arr1[j] << std::endl;

	std::cout << std::endl;
	Array<int> arr2 = Array<int>(arr1);

	arr2[0] = 8732;

	for (unsigned int j = 0; j < arr2.size(); j++)
		std::cout << arr2[j] << std::endl;

	std::cout << std::endl;
	Array<std::string> arr_s = Array<std::string>(i);
	arr_s[0] = "1st string";
	arr_s[1] = "2nd string";
	arr_s[2] = "3rd string";
	arr_s[3] = "4th string";
	arr_s[4] = "5th string";
	arr_s[5] = "6th string";
	arr_s[6] = "7th string";
	try
	{
		arr_s[7] = "8th string";
	}
	catch (std::exception &e)
	{
		std::cout << e.what() << std::endl;
	}

	try
	{
		arr_s[9] = "9th string";
	}
	catch (std::exception &e)
	{
		std::cout << e.what() << std::endl;
	}
	for (unsigned int j = 0; j < arr2.size(); j++)
		std::cout << arr_s[j] << std::endl;
	return 0;
}
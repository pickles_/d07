//
// Created by dtitenko on 11/9/17.
//

#include <cstdlib>
#include <iostream>

template <class T>
void iter(T *array, size_t len, void (*f)(const T &))
{
	for (size_t i = 0; i < len; i++)
		f(array[i]);
}

template <class T>
void print(const T &val)
{
	std::cout << val << std::endl;
}

int main()
{
	float foo[] = {42.42, 4.2, 42.21, 0.42, 42.0};
	std::string bar[] = {"UNIT Factory", "42 Kyiv", "UNIT.City"};

	size_t foo_size = sizeof(foo) / sizeof(foo[0]);
	size_t bar_size = sizeof(bar) / sizeof(bar[0]);

	iter(foo, foo_size, print);
	std::cout << std::endl;
	iter(bar, bar_size, print);
}